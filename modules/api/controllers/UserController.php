<?php

namespace app\modules\api\controllers;

use app\modules\api\models\Login;
use Yii;
use app\modules\api\models\RegisterModel;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\Response;

class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    /**
     * POST
     */
    public function actionRegister()
    {
        $request = Yii::$app->request;
        $model = new RegisterModel();

        if($model->load($request->get())) {

            $token = $model->register();

            if($token == null){
                return $model->errors;
            }

            return ['result' => $token];
        }

        return ['result' => false];
    }

    /**
     * POST
     */
    public function actionGetToken()
    {
        $request = Yii::$app->request;
        $model = new Login();

        if($model->load($request->get())) {

            $token = $model->login();

            if($token == null){
                return $model->errors;
            }

            return ['result' => $token];
        }

        return ['result' => false];
    }
}