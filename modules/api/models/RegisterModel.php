<?php

namespace app\modules\api\models;

use app\models\MobileUser;
use yii\base\Model;

class RegisterModel extends Model
{
    public $login;
    public $password;
    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login', 'password', 'email'], 'required'],
            [['login', 'password'], 'string'],
            [['email'], 'email'],
            ['email', 'unique', 'targetClass' => MobileUser::className(), 'targetAttribute' => 'email'],
        ];
    }

    /**
     * @return string
     */
    public function register()
    {
        if($this->validate()){
            $user = new MobileUser([
                'login' => $this->login,
                'password' => $this->password,
                'email' => $this->email,
            ]);
            $user->save(false);

            return $user->token;
        }

        return null;
    }
}