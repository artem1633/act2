<?php

namespace app\commands;

use app\models\User;
use Faker\Factory;
use yii\console\Controller;
use yii\helpers\ArrayHelper;

/**
 * Class UserController
 * @package app\commands
 */
class UserController extends Controller
{
    /**
     *
     */
    public function actionFactory()
    {
        $pks = ArrayHelper::getColumn(User::find()->all(), 'id');
        for ($i = 0; $i < 60; $i++)
        {
            $factory = Factory::create('Ru_RU');

            if(count($pks) == 0){
                $refId = null;
            } else {
                $refId = $pks[rand(0, count($pks)-1)];
            }

            $user = new User([
                'name' => $factory->name,
                'login' => $factory->email,
                'password' => 'qwerty',
                'balance_usd' => $factory->randomFloat(2, 10, 2000),
                'country' => $factory->country,
                'living_place' => $factory->city,
                'phone' => $factory->phoneNumber,
                'profession' => $factory->jobTitle,
                'ref_id' => $refId,
                'role' => User::ROLE_USER,
            ]);

            $user->save(false);

            $pks[] = $user->id;
        }
    }
}