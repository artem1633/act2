<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Companies;

/**
 * CompaniesSearch represents the model behind the search form about `app\models\Companies`.
 */
class CompaniesSearch extends Companies
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'is_super', 'is_activity'], 'integer'],
            [['company_name', 'created_date', 'last_activity_datetime', 'access_end_datetime'], 'safe'],
            [['rate_a', 'rate_b', 'rate_c', 'rate_d', 'credit'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Companies::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'created_date' => $this->created_date,
            'rate_a' => $this->rate_a,
            'rate_b' => $this->rate_b,
            'rate_c' => $this->rate_c,
            'rate_d' => $this->rate_d,
            'credit' => $this->credit,
            'last_activity_datetime' => $this->last_activity_datetime,
            'access_end_datetime' => $this->access_end_datetime,
            'is_super' => $this->is_super,
            'is_activity' => $this->is_activity,
        ]);

        $query->andFilterWhere(['like', 'company_name', $this->company_name]);

        return $dataProvider;
    }
}
