<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "accruals".
 *
 * @property int $id
 * @property string $date дата и время начисления
 * @property int $from_company_id Откуда
 * @property int $for_company_id Куда
 * @property double $amount сумма начисления
 * @property int $user_level уровень тому кому начислено
 *
 * @property Companies $forCompany
 * @property Companies $fromCompany
 */
class Accruals extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'accruals';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date'], 'safe'],
            [['from_company_id', 'for_company_id', 'user_level'], 'integer'],
            [['amount'], 'number'],
            [['for_company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Companies::className(), 'targetAttribute' => ['for_company_id' => 'id']],
            [['from_company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Companies::className(), 'targetAttribute' => ['from_company_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'дата и время начисления',
            'from_company_id' => 'от кого',
            'for_company_id' => 'кому',
            'amount' => 'сумма начисления',
            'user_level' => 'уровень тому кому начислено',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForCompany()
    {
        return $this->hasOne(Companies::className(), ['id' => 'for_company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFromCompany()
    {
        return $this->hasOne(Companies::className(), ['id' => 'from_company_id']);
    }
}
