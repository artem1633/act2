<?php
namespace app\models\forms;

use app\models\Accruals;
use app\models\Companies;
use app\models\RefAgent;
use app\models\Settings;
use yii\base\Model;
use app\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{

    public $email;
    public $password;
    public $ref_id;
    public $living_country;
    public $name;
    public $birth_date;
    public $country;
    public $living_place;
    public $education;
    public $profession;
    public $work_place;
    public $work_position;
    public $work_status;
    public $phone;
    public $interests;
    public $hobby;
//    public $rateA;
//    public $rateB;
//    public $rateC;
//    public $rateD;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name','email', 'password', 'ref_id'], 'required'],
            ['email', 'trim'],
            ['email', 'email'],
            [['ref_id'], 'integer'],
            ['email', 'unique', 'targetClass' => '\app\models\User', 'targetAttribute' => 'login', 'message' => 'Этот email уже зарегистрирован'],
            ['password', 'string', 'min' => 6],
            [['name', 'birth_date', 'country', 'living_place', 'education', 'profession', 'living_country', 'work_place', 'work_position', 'work_status', 'phone', 'interests', 'hobby'], 'string', 'max' => 255],
            ['ref_id', function(){
                $company = Companies::findOne($this->ref_id);

                if($company == null){
                    $this->addError('ref_id', 'Реферальная ссылка не корректная');
                }
            }]
//            [['rateA', 'rateB', 'rateC', 'rateD'], 'number'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => 'E-mail',
            'password' => 'Пароль',
            'repeatPassword' => 'Повторите пароль',
            'ref_id' => 'Реферальная ссылка',
            'name' => 'Ф.И.О.',
            'birth_date' => 'Дата рождения',
            'country' => 'Ваше гражданство',
            'living_country' => 'Страна',
            'living_place' => 'Город',
            'education' => 'Ваше образование',
            'profession' => 'Ваша профессия',
            'work_place' => 'Ваше место работы',
            'work_position' => 'Ваша должность',
            'work_status' => 'Интересует ли Вас возможность обучать других мастеров',
            'phone' => 'Номер телефона',
            'interests' => 'Круг Ваших интересов',
            'hobby' => 'Ваше (Ваши) хобби',
            'rateA' => 'Уровень 1',
            'rateB' => 'Уровень 2',
            'rateC' => 'Уровень 3',
            'rateD' => 'Уровень 4',
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }


        $company = new Companies([
            'company_name' => $this->name,
            'created_date' => date('Y-m-d H:i:s'),
            'ref_id' => $this->ref_id,
            'rate_a' => Settings::find()->where(['key' => "level_1"])->one()->value,
            'rate_b' => Settings::find()->where(['key' => "level_2"])->one()->value,
            'rate_c' => Settings::find()->where(['key' => "level_3"])->one()->value,
            'rate_d' => Settings::find()->where(['key' => "level_4"])->one()->value,
//            'rate_a' => $this->rateA,
//            'rate_b' => $this->rateB,
//            'rate_c' => $this->rateC,
//            'rate_d' => $this->rateD,
            'credit' => 0,
            'is_activity' => 1,
        ]);
        $company->save(false);

        $user = new User();
        $user->name = $this->name;
        $user->login = $this->email;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->password = $this->password;
        $user->role = User::ROLE_MANAGER;
        $user->birth_date = $this->birth_date;
        $user->living_country = $this->living_country;
        $user->country =  $this->country;
        $user->living_place = $this->living_place;
        $user->education = $this->education;
        $user->profession = $this->profession;
        $user->work_place = $this->work_place;
        $user->work_position = $this->work_position;
        $user->work_status = $this->work_status;
        $user->phone = $this->phone;
        $user->interests = $this->interests;
        $user->hobby = $this->hobby;
//        $user->ref_id = $this->ref_id;
        $user->company_id = $company->id;
        $user->save(false);

//        $this->levelRecursive($user);

        return $user;
    }

    public function levelRecursive($user, $counter = 1)
    {
        if($counter > 7){
            return false;
        }

        $level = Settings::find()->where(['key' => "level_$counter"])->one()->value;

        $parentUser = User::find()->where(['id' => $user->ref_id])->one();

        if($parentUser){
            $parentUser->balance_usd = $parentUser->balance_usd + $level;
            $parentUser->save(false);

            $accruals = new Accruals([
                'amount' => $level,
                'from_user_id' => $user->id,
                'for_user_id' => $parentUser->id,
                'user_level' => $counter,
                'date' => date('Y-m-d H:i:s'),
            ]);
            $accruals->save(false);

            $parentUser2 = User::find()->where(['id' => $parentUser->ref_id])->one();

            if($parentUser2 == null){
                return true;
            } else {
                $this->levelRecursive($parentUser, $counter+1);
            }
        }

        return true;
    }

//    /**
//     * update user.
//     *
//     * @param User $user
//     * @return User|null the saved model or null if saving fails
//     */
//    public function update($user)
//    {
//        if (!$this->validate()) {
//            return null;
//        }
//
//        $user->name = $this->name;
//        $user->surname = $this->surname;
//        $user->patronymic = $this->patronymic;
//        $user->phone = $this->phone;
//        $user->category = $this->category;
//        $user->department = $this->department;
//        $user->email = $this->email;
//        $user->setPassword($this->password);
//        $user->password = $this->password;
//        // $user->generateAuthKey();
//
//        return $user->update();
//    }
}
