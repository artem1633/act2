<?php

namespace app\models\forms;

use Yii;
use yii\base\Model;
use app\models\Accruals;
use app\models\Operations;
use yii\helpers\VarDumper;


/**
 * Class ReportForm
 * @package app\models\forms
 */
class ReportForm extends Model
{
    /**
     * @var string
     */
    public $dateStart;

    /**
     * @var string
     */
    public $dateEnd;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['dateStart', 'dateEnd'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if($this->dateStart == null || $this->dateEnd == null){
            $this->dateStart = date('Y-m-d', (time() - (86400 * 30)));
            $this->dateEnd = date('Y-m-d');
        }
    }

    /**
     * @return array
     */
    public function report()
    {
        $subscribersReport = [];
        if(Yii::$app->user->identity->isSuperAdmin() || Yii::$app->user->identity->isManager()){
            $historys = Accruals::find()
                ->where(['for_company_id' => Yii::$app->user->identity->company_id])
                ->andFilterWhere(['between', 'date', "{$this->dateStart} 00:00:00", "{$this->dateEnd} 23:59:59"])
                ->all();

            if($historys){
                foreach ($historys as $history) {
                    $subscribersReport['date'][] = $history->date;
                    $subscribersReport['credit'][] = $history->amount;
                }
            } else {
                $subscribersReport['date'][] = '';
                $subscribersReport['credit'][] = '';
            }
        } else if(Yii::$app->user->identity->isShop()) {
            $historys = Operations::find()
                ->where(['user_id' => Yii::$app->user->identity->id])
                ->andFilterWhere(['between', 'created_at', "{$this->dateStart} 00:00:00", "{$this->dateEnd} 23:59:59"])
                ->all();

            if($historys){
                foreach ($historys as $history) {
                    $subscribersReport['date'][] = $history->created_at;
                    $subscribersReport['credit'][] = $history->sum;
                }
            } else {
                $subscribersReport['date'][] = '';
                $subscribersReport['credit'][] = '';
            }
        }

        return $subscribersReport;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'dateStart' => 'Дата С',
            'dateEnd' => 'Дата по',
        ];
    }
}