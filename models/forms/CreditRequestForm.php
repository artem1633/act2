<?php

namespace app\models\forms;

use app\models\Settings;
use SendGrid\Mail\Mail;
use yii\base\Model;

/**
 * Class CreditRequestForm
 * @package app\models\forms
 */
class CreditRequestForm extends Model
{
    /**
     * @var string
     */
    public $text;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'text' => 'Текст'
        ];
    }

    /**
     * @return boolean
     */
    public function send()
    {
        if($this->validate()){

            $adminMail = Settings::findByKey('admin_email');

            $company = \Yii::$app->user->identity->company;

            if($adminMail != null){
                $email = new Mail();
                $sendgrid = new \SendGrid('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');
                $email->setFrom('no-reply@act.com', 'Act notification service');
                $email->setSubject('Запрос на вывод');
                $email->addTo($adminMail->value);
                $email->addContent(
                    'text/html',
                    "Запрос пришел от компании «{$company->company_name}» (ID: {$company->id})"."<br>Текст сообщения от компании: «{$this->text}»"
                );
                $sendgrid->send($email);

                return true;
            }
        }

        return false;
    }
}