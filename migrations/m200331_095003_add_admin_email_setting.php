<?php

use yii\db\Migration;

/**
 * Class m200331_095003_add_admin_email_setting
 */
class m200331_095003_add_admin_email_setting extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('settings', [
            'key' => 'admin_email',
            'label' => 'Почта администратора'
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        \app\models\Settings::deleteAll(['key' => 'admin_email']);
    }
}
