<?php

use yii\db\Migration;

/**
 * Class m200214_163320_accruals
 */
class m200214_163320_accruals extends Migration
{
    public function up()
    {
        $this->createTable('accruals', [
            'id' => $this->primaryKey(),
            'date' => $this->dateTime()->comment('дата и время начисления'),
            'from_user_id' => $this->integer()->comment('от кого'),
            'for_user_id' => $this->integer()->comment('кому'),
            'amount' => $this->float()->comment('сумма начисления'),
            'user_level' => $this->integer()->comment('уровень тому кому начислено'),
        ]);

        $this->createIndex(
            'idx-accruals-from_user_id',
            'accruals',
            'from_user_id'
        );

        $this->addForeignKey(
            'fk-accruals-from_user_id',
            'accruals',
            'from_user_id',
            'user',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-accruals-for_user_id',
            'accruals',
            'for_user_id'
        );

        $this->addForeignKey(
            'fk-accruals-for_user_id',
            'accruals',
            'for_user_id',
            'user',
            'id',
            'SET NULL'
        );

//        $this->addForeignKey('fk_from_user_id_to_user',
//        'accruals', ['from_user_id'],
//        'user',['id']
//        );
//
//        $this->addForeignKey('fk_for_user_id_to_user',
//        'accruals', ['for_user_id'],
//        'user',['id']
//    );
    }

    public function down()
    {
        $this->dropForeignKey(
            'fk-accruals-for_user_id',
            'accruals'
        );

        $this->dropIndex(
            'idx-accruals-for_user_id',
            'accruals'
        );

        $this->dropForeignKey(
            'fk-accruals-from_user_id',
            'accruals'
        );

        $this->dropIndex(
            'idx-accruals-from_user_id',
            'accruals'
        );

        $this->dropTable('accruals');
    }
}
