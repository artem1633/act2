<?php

use yii\db\Migration;

/**
 * Handles adding living_country to table `user`.
 */
class m200307_140717_add_living_country_column_to_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user', 'living_country', $this->string()->comment('Страна'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user', 'living_country');
    }
}
