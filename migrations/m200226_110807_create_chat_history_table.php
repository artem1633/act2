<?php

use yii\db\Migration;

/**
 * Handles the creation of table `chat_history`.
 */
class m200226_110807_create_chat_history_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('chat_history', [
            'id' => $this->primaryKey(),
            'sender_id' => $this->integer()->comment('Отправитель'),
            'recipient_id' => $this->integer()->comment('Получатель'),
            'text' => $this->binary()->comment('Текст сообщения'),
            'message_send_datetime' => $this->dateTime()->comment('Дата и время отправки сообщения'),
        ]);

        $this->createIndex(
            'idx-chat_history-sender_id',
            'chat_history',
            'sender_id'
        );

        $this->addForeignKey(
            'fk-chat_history-sender_id',
            'chat_history',
            'sender_id',
            'user',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-chat_history-recipient_id',
            'chat_history',
            'recipient_id'
        );

        $this->addForeignKey(
            'fk-chat_history-recipient_id',
            'chat_history',
            'recipient_id',
            'user',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-chat_history-recipient_id',
            'chat_history'
        );

        $this->dropIndex(
            'idx-chat_history-recipient_id',
            'chat_history'
        );

        $this->dropForeignKey(
            'fk-chat_history-sender_id',
            'chat_history'
        );

        $this->dropIndex(
            'idx-chat_history-sender_id',
            'chat_history'
        );

        $this->dropTable('chat_history');
    }
}
