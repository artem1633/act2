<?php

use yii\db\Migration;

/**
 * Handles the creation of table `news`.
 */
class m190815_171219_create_news_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('news', [
            'id' => $this->primaryKey(),
            'subject' => $this->string()->comment('Заголовок'),
            'content' => $this->text()->comment('Текст'),
            'views_count' => $this->integer()->unsigned()->comment('Кол-во просмотров'),
            'created_at' => $this->dateTime()->comment('Дата и время создания'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('news');
    }
}
