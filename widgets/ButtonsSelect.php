<?php

namespace app\widgets;

use yii\base\InvalidConfigException;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\InputWidget;

/**
 * Class ButtonsSelect
 * @package app\widgets
 */
class ButtonsSelect extends InputWidget
{
    /**
     * @var array
     */
    public $data;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if($this->data == null){
            throw new InvalidConfigException('data must be required');
        }
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        parent::run();

        $dropDownId = "{$this->id}dd";
        $dropDown = '<div class="hidden">'.Html::activeDropDownList($this->model, $this->attribute, $this->data, ['id' => $dropDownId]).'</div>';

        $buttons = '';

        $attribute = $this->attribute;
        $value = $this->model->$attribute;

        if(count($this->data) > 1){
            $data = $this->data;
            reset($data);
            $value = key($data);
        }

        foreach ($this->data as $key => $text){
            $class = 'btn btn-default';
            if($value == $key){
                $class = 'btn btn-default active';
            }
            $buttons .= Html::button($text, ['class' => $class, 'data-key' => $key]);
        }

        $buttonsGroup = "<div id='{$this->id}' class='btn-group' data-attribute='' role='group'>{$buttons}</div>";

        $this->view->registerJs("
            $('#{$this->id} button').click(function(){
                var value = $(this).data('key');
                $('#{$this->id} button.active').removeClass('active');
                
                $(this).addClass('active');
                
                $('#{$dropDownId}').val(value);
                $('#{$dropDownId}').trigger('change');
            });
        ", View::POS_READY);

        return "<div style='display: block;'>{$buttonsGroup}{$dropDown}</div>";
    }
}