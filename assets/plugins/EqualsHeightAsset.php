<?php

namespace app\assets\plugins;

use yii\web\AssetBundle;

class EqualsHeightAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
    ];
    public $js = [
        'libs/equalsHeights/jquery.equalheights.min.js',
    ];
    public $depends = [
        'app\assets\AppAsset',
    ];
}