<?php

use app\models\User;
use yii\widgets\ActiveForm;

/**
 * @var $model User
 */


$this->title = 'Профиль';

?>

    <style>
        .board .form-group label.control-label {
            font-weight: 400;
            font-size: 10px;
            line-height: 1.2;
            margin-bottom: 6px;
            color: #5F9ABB;
        }

        .board .form-group input{
            width: 100%;
            height: 26px;
            padding: 4px 12px;
            background: #ffffff;
            border: 1px solid #C4C4C4;
            border-radius: 3px;
        }
    </style>

    <div class="board board_main">
        <div class="board-btns"><span class="board__title masters">Настройки</span>
        </div>
        <div class="board__wrap">
            <?php $form = ActiveForm::begin(['options' => ['class' => 'form']]) ?>

            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-12">
                            <?= $form->field($model, 'name')->textInput() ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'login')->textInput() ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'password')->passwordInput() ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'phone')->textInput() ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'birth_date')->input('date') ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'country')->textInput() ?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'living_country')->textInput() ?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'living_place')->textInput() ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'education')->textInput() ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'profession')->textInput() ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'work_place')->textInput() ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'work_position')->textInput() ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'interests')->textInput() ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'hobby')->textInput() ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <?= $form->field($model, 'work_status', ['options' => ['class' => '']])->radioList(['да' => 'да',
                                'нет' => 'нет',
                                'возможно' => 'возможно',
                            ],
                                [
                                    'item' => function($index, $label, $name, $checked, $value) {

                                        $return = '<input class="" id="'.$index.'" type="radio" name="' . $name . '" value="' . $value . '" tabindex="3">';
                                        $return .= '<label class="form__label form__label_radio" for="'.$index.'">';
                                        $return .= ucwords($label);
                                        $return .= '</label>';

                                        return $return;
                                    }
                                ]) ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <?= \yii\helpers\Html::submitButton('<i class="fa fa-check"></i> Сохранить', ['class' => 'btn-form lk']) ?>
                        </div>
                    </div>

                </div>

                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-12">
                            <img class="circle-img" style="height: 250px; width: 250px; border-radius: 100%; object-fit: contain; border: 2px solid #cecece; cursor: pointer;" src="/<?=$model->getRealAvatarPath()?>" data-role="profile-image-select">
                        </div>
                    </div>
                </div>
            </div>

            <?php ActiveForm::end() ?>
        </div>
    </div>

<?php

$script = <<< JS
    $('[data-role="profile-image-select"]').click(function(){
        $('#avatar-form input').trigger('click');
    });

    $('#avatar-form input').change(function(){
        $('#avatar-form').submit();
    });
    
    $('#avatar-form').submit(function(e){
        var formData = new FormData(this);
        $.ajax({
            type: "POST",
            url: $('#avatar-form').attr('action'),
            data: formData,
            contentType: false,
            cache: false,
            processData: false,
            success: function(response){
                if(response.success === 1){
                    var path = '/'+response.path;
                    $('[data-role="avatar-view"]').each(function(i){
                        $(this).attr('src', path);
                    });
                    $('[data-role="profile-image-select"]').each(function(i){
                        $(this).attr('src', path);
                    });
                }
            }
        });
        e.preventDefault();
    });
JS;

$this->registerJs($script, \yii\web\View::POS_READY);


?>