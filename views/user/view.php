<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = "Пользователь «{$model->name}»";

?>
<div class="user-view">

    <div class="panel panel-warning">
        <div class="panel-heading">
            <h4 class="panel-title">Пользователь</h4>
        </div>
        <div class="panel-body">
            <div class="col-md-9">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'name',
                        'birth_date',
                        'country',
                        'living_place',
                        'education',
                        'profession',
                        'work_place',
                        'work_position',
                        'work_status',
                        'phone',
                        'interests',
                        'hobby',
                        'role',
                        'balance_usd',
                        'last_active_datetime',
                        'created_at',
                    ],
                ]) ?>
            </div>
            <div class="col-md-3">
                <img src="/<?=$model->getRealAvatarPath()?>" class="circle-img" style="height: 250px; width: 250px; border-radius: 100%; object-fit: contain; border: 2px solid #cecece;">
            </div>
        </div>
    </div>

</div>
