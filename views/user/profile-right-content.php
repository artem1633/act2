<?php

use \app\models\Companies;

/** @var $this \yii\web\View */
/** @var $model \app\models\User */

$company = Companies::findOne($model->company_id);

$refParentName = null;

if($model->ref_id){
    $refParentCompany = Companies::findOne($model->ref_id);

    if($refParentCompany){
        $refParentName = $refParentCompany->company_name;
    }
}

?>


<div class="bar-panel">
    <div class="bar bar_select">
        <div class="bar__wrap">
            <div class="bar__row">
                <div class="bar__icon"> <img src="/<?=$model->getRealAvatarPath()?>" alt=""></div><span class="bar__text"><?= $model->login ?></span><span class="bar__arrow"></span>
            </div>
            <div class="bar__drop">
                <div class="bar__drop-wrap"><a class="bar__text" href="#">Счет</a><a class="bar__text" href="#">Профиль</a><a class="bar__text" data-method="post" href="<?= \yii\helpers\Url::toRoute(['site/logout']) ?>">Выйти</a></div>
            </div>
        </div>
    </div>
    <div class="bar bar_info">
        <div class="bar__wrap"> <span class="bar__text-credit">Кредиты <?= $model->balance_usd ?></span></div><span class="bar__text center">Дата регистрации <?= $model->created_at ?></span>
    </div>
    <div class="bar bar_info">
        <div class="bar__wrap"> <span class="bar__text">Логин: <?= $model->login ?></span><span class="bar__text">Приглашенных: <?= Companies::find()->where(['ref_id' => Yii::$app->user->identity->company_id])->count() ?></span><span class="bar__text">Приглашен от: <?=$refParentName?></span></div>
    </div><a class="btn-form primary" href="#">Пригласить друга </a>
</div>