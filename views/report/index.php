<?php

/** @var $this yii\web\View */
/** @var $report array */

use skeeks\widget\highcharts\Highcharts;
use yii\helpers\Html;

$this->title = 'Отчет';

// \yii\helpers\VarDumper::dump($report, 10, true);
// exit;

if(isset($report['data']) == false){
    $report['data'] = null;
}

$fieldOptions1 = [
    'inputTemplate' => "{input}"
];

?>

<?php \yii\widgets\Pjax::begin(['id' => 'pjax-chart-container']) ?>

    <div class="board board_main"><span class="board__title">Статистика</span>
        <div class="board__wrap">
            <form class="stat-form" action="#">
                <div class="stat-form__wrap">
                    <?php $form = \yii\bootstrap\ActiveForm::begin(['id' => 'search-form', 'method' => 'GET']) ?>
                            <?= $form->field($searchModel, 'dateStart', $fieldOptions1)->input('date', ['class' => 'stat-form__input'])->label(false) ?>
                            <?= $form->field($searchModel, 'dateEnd', $fieldOptions1)->input('date', ['class' => 'stat-form__input'])->label(false) ?>
                            <?= Html::submitButton('', ['class' => 'stat-form__btn', 'style' => 'margin-top: -30px;']) ?>
                    <?php \yii\bootstrap\ActiveForm::end() ?>
                </div>
            </form>
        </div>
    </div>


<?php \yii\widgets\Pjax::end() ?>


    <?php if(Yii::$app->user->identity->isSuperAdmin() || Yii::$app->user->identity->isManager()): ?>
    <div class="board"><span class="board__title">Мой доход </span>
        <div class="board__wrap">
            <div class="board__row board__head"><span class="board__text">Текущая неделя </span><span class="board__text">Текущий месяц</span><span class="board__text">За все время </span></div>
            <div class="board__row"><span class="board__count"><?= $creditWeek ?></span><span class="board__count"><?= $creditMonth ?></span><span class="board__count"><?= $creditAll ?></span></div>
        </div>
    </div>


    <div class="board"><span class="board__title">Мой доход с линии В </span>
        <div class="board__wrap">
            <div class="board__row board__head"><span class="board__text">Текущая неделя </span><span class="board__text">Текущий месяц</span><span class="board__text">За все время </span></div>
            <div class="board__row"><span class="board__count"><?= $creditWeekB ?></span><span class="board__count"><?= $creditMonthB ?></span><span class="board__count"><?= $creditAllB ?></span></div>
        </div>
    </div>



    <div class="board"><span class="board__title">Мой доход с линии C</span>
        <div class="board__wrap">
            <div class="board__row board__head"><span class="board__text">Текущая неделя </span><span class="board__text">Текущий месяц</span><span class="board__text">За все время </span></div>
            <div class="board__row"><span class="board__count"><?= $creditWeekC ?></span><span class="board__count"><?= $creditMonthC ?></span><span class="board__count"><?= $creditAllC ?></span></div>
        </div>
    </div>


    <div class="board"><span class="board__title">Мой доход с линии D</span>
        <div class="board__wrap">
            <div class="board__row board__head"><span class="board__text">Текущая неделя </span><span class="board__text">Текущий месяц</span><span class="board__text">За все время </span></div>
            <div class="board__row"><span class="board__count"><?= $creditWeekD ?></span><span class="board__count"><?= $creditMonthD ?></span><span class="board__count"><?= $creditAllD ?></span></div>
        </div>
    </div>


    <?php elseif(Yii::$app->user->identity->isShop()): ?>

    <div class="board"><span class="board__title">Мой доход с линии D</span>
        <div class="board__wrap">
            <div class="board__row board__head"><span class="board__text">Текущая неделя </span><span class="board__text">Текущий месяц</span><span class="board__text">За все время </span></div>
            <div class="board__row"><span class="board__count"><?= $debitWeek ?></span><span class="board__count"><?= $debitMonth ?></span><span class="board__count"><?= $debitAll ?></span></div>
        </div>
    </div>


    <?php endif; ?>

<?php

$script = <<< JS
    $('#filter-form input').change(function(){
        $('#filter-form').submit();
    });
JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>