<?php

use app\models\UserSearch;
use yii\widgets\ActiveForm;
use app\helpers\AvatarResolver;
use yii\helpers\Html;
use app\models\User;
use yii\helpers\Url;

/** @var $dataProvider \yii\data\ActiveDataProvider */

?>

    <?php

        if(Yii::$app->user->isGuest == false){
            $searchModel = new UserSearch();
            $dataProvider = $searchModel->searchChild(Yii::$app->request->queryParams);
            $dataProvider->pagination = false;

            $refersCount = \app\models\Companies::find()->where(['ref_id' => Yii::$app->user->identity->company_id])->count();

        }
    ?>

<?php if(Yii::$app->user->isGuest == false): ?>
    <div class="theme-panel active" style="top: 40px; height: 100%; z-index: 1; background: #efcb5b;">
        <!--            <a href="javascript:;" data-click="theme-panel-expand" class="theme-collapse-btn"><i class="fa fa-cog"></i></a>-->



        <p style="font-size: 15px;">Дата регистрации: <b><?= Yii::$app->user->identity->created_at ?></b></p>
        <p style="font-size: 15px;">Логин: <b><?= Yii::$app->user->identity->login ?></b></p>
        <?php if(Yii::$app->user->identity->isSuperAdmin() || Yii::$app->user->identity->isManager()): ?>
            <p style="font-size: 15px;">Приглашенных: <b><?= $refersCount ?></b></p>
        <?php endif; ?>
        <?php if(Yii::$app->user->identity->ref_id != null): ?>
            <?php $user = \app\models\User::findOne(Yii::$app->user->identity->ref_id); ?>
            <p style="font-size: 15px;">Приглашен от <b><?= $user->name ?></b></p>
        <?php elseif(Yii::$app->user->identity->company->ref_id != null): ?>
            <?php $company = \app\models\Companies::findOne(Yii::$app->user->identity->company->ref_id); ?>
            <p style="font-size: 15px;">Приглашен от <b><?= $company->company_name ?></b></p>
        <?php endif; ?>
        <p>
            <?php if(Yii::$app->user->identity->isManager()): ?>
                <?= Html::a('Пригласить друга', '#', ['class' => 'btn btn-success btn-block', 'data-ref' => Url::toRoute(['site/register', 'ref_id' => Yii::$app->user->identity->company->id], true), 'onclick' => '
                var str = $(this).attr("data-ref");
                copyToClipboard(str);
                alert("Скопировано");
            ']) ?>
            <?php elseif(Yii::$app->user->identity->isManager() == false): ?>
                <?= Html::a('Зафиксировать продажу', ['operations/index'], ['class' => 'btn btn-success btn-block']) ?>
            <?php endif; ?>
        </p>

        <?= Html::a('<img style="width: 100%; object-fit: cover;" src="/img/advert.jpg">', '#') ?>


    </div>

<?php endif; ?>
        <div class="">
            <div class="">
                <div class="row">


                </div>
            </div>
        </div>

    </div>
        </div>



<?php

$script = <<< JS

$('#user-list-form input').change(function(){
    $('#user-list-form').submit();
});

JS;

$this->registerJs($script, \yii\web\View::POS_READY);


?>
