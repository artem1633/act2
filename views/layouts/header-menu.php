<?php

use app\admintheme\widgets\Menu;
use yii\helpers\Html;

$level1Count = 0;
$level2Count = 0;
$level3Count = 0;
$level4Count = 0;
$level5Count = 0;
$level6Count = 0;
$level7Count = 0;

$url = 1;
if (Yii::$app->user->isGuest == false) {
    $levels = Yii::$app->user->identity->refer->getLevelsCount();
    $url = Yii::$app->user->identity->getSelfRefHref();
    $level1Count = $levels['1'];
    $level2Count = $levels['2'];
    $level3Count = $levels['3'];
    $level4Count = $levels['4'];
    $level5Count = $levels['5'];
    $level6Count = $levels['6'];
    $level7Count = $levels['7'];
}

?>

<div id="sidebar" class="sidebar" style="padding-top: 0; z-index: 2000;">

    <div class="sidebar-header" style="font-size: 29px!important;">
        <?= Html::a('  ', ['dashboard/index'],['style' => 'font-size:29px']) ?>
    </div>

    <?php if (Yii::$app->controller->id != 'chat'): ?>
    <div class="left-menu">

        <?php if (Yii::$app->user->isGuest == false): ?>
            <?php
            echo Menu::widget(
                [
                    'options' => ['class' => 'nav', 'style' => 'margin-top: 20px;'],
                    'items' => [
                        [
                            'label' => 'Компании',
                            'icon' => 'fa  fa-users',
                            'url' => ['/companies/index'],
                            'visible' => Yii::$app->user->identity->isSuperAdmin()
                        ],
                        [
                            'label' => 'Финансы',
                            'icon' => 'fa  fa-users',
                            'url' => ['/report/index'],
                        ],
                        [
                            'label' => 'Команда',
                            'icon' => 'fa  fa-users',
                            'url' => ['/dashboard/index'],
                            'visible' => Yii::$app->user->identity->isManager()
                        ],
                        [
                            'label' => 'История продаж',
                            'icon' => 'fa fa-rub',
                            'url' => ['/operations/index'],
                            'visible' => (Yii::$app->user->identity->isShop() || Yii::$app->user->identity->isSuperAdmin())
                        ],
                        [
                            'label' => 'Магазины',
                            'icon' => 'fa  fa-users',
                            'url' => ['/user/index'],
                            'visible' => (Yii::$app->user->identity->isManager() || Yii::$app->user->identity->isSuperAdmin())
                        ],
                        [
                            'label' => 'Вывод Credit',
                            'icon' => 'fa  fa-users',
                            'url' => ['/credit-request/index'],
                            'visible' => (Yii::$app->user->identity->isManager() || Yii::$app->user->identity->isSuperAdmin())
                        ],
                        [
                            'label' => 'Контакты',
                            'icon' => 'fa  fa-user-o',
                            'url' => ['/contacts/index'],
                        ],
                        [
                            'label' => 'База знаний',
                            'icon' => 'fa  fa-user-o',
                            'url' => 'https://google.com',
                        ],
//                        [
//                            'label' => 'Новости',
//                            'icon' => 'fa  fa-user-o',
//                            'url' => ['/news'],
//                        ],
                        [
                            'label' => 'Начисления',
                            'icon' => 'fa  fa-plus',
                            'url' => ['/accruals/index'],
                            'visible' => Yii::$app->user->identity->isSuperAdmin()
                        ],
                        [
                            'label' => 'Настройки',
                            'icon' => 'fa  fa-cog',
                            'url' => ['/settings'],
                            'visible' => Yii::$app->user->identity->isSuperAdmin()
                        ],
//                    ['label' => 'Тикеты', 'icon' => 'fa  fa-pencil', 'url' => ['/ticket'],],
//                    ['label' => 'Новости', 'icon' => 'fa  fa-th', 'url' => ['/news'],],
//                    ['label' => 'ЧАВО', 'icon' => 'fa  fa-th', 'url' => ['/faq'],],
                    ],
                ]
            );
            ?>
            <?= Html::a('<img style="width: 100%; object-fit: cover; margin-top: 20px;" src="/img/advert.jpg">', '#') ?>

        <?php endif; ?>
    </div>
    <?php else: ?>
        <?= $this->render('/chat/partner_list.php') ?>
    <?php endif; ?>
</div>
