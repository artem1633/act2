<?php

use yii\helpers\Url;

/** @var $this \yii\web\View */

/**
 * @return string
 */
function isActive($controllerId)
{
    if(Yii::$app->controller->id == $controllerId){
        return " active";
    } else {
        return "";
    }

}

?>

<ul class="lk-list">
    <?php if(Yii::$app->user->identity->role == \app\models\User::ROLE_MANAGER): ?>
        <li><a class="btn-form primary secondary<?=isActive('report')?>" href="<?= Url::toRoute(['report/index']) ?>">Финансы</a></li>
        <li><a class="btn-form primary secondary<?=isActive('dashboard')?>" href="<?= Url::toRoute(['dashboard/index']) ?>">Команда</a></li>
        <li><a class="btn-form primary secondary<?=isActive('user')?>" href="<?= Url::toRoute(['user/index']) ?>">Парикмахерская</a></li>
        <li><a class="btn-form primary secondary<?=isActive('credit-request')?>" href="<?= Url::toRoute(['credit-request/index']) ?>">Вывод Credit</a></li>
    <?php elseif(Yii::$app->user->identity->role == \app\models\User::ROLE_USER): ?>
        <li><a class="btn-form primary secondary<?=isActive('report')?>" href="<?= Url::toRoute(['report/index']) ?>">Финансы</a></li>
        <li><a class="btn-form primary secondary<?=isActive('operations')?>" href="<?= Url::toRoute(['operations/index']) ?>">История продаж</a></li>
    <?php else: ?>
        <li><a class="btn-form primary secondary<?=isActive('report')?>" href="<?= Url::toRoute(['report/index']) ?>">Финансы</a></li>
        <li><a class="btn-form primary secondary<?=isActive('companies')?>" href="<?= Url::toRoute(['companies/index']) ?>">Команда</a></li>
        <li><a class="btn-form primary secondary<?=isActive('operations')?>" href="<?= Url::toRoute(['operations/index']) ?>">История продаж</a></li>
        <li><a class="btn-form primary secondary<?=isActive('user')?>" href="<?= Url::toRoute(['user/index']) ?>">Пользователи</a></li>
        <li><a class="btn-form primary secondary" style="display: none;" href="<?= Url::toRoute(['report/index']) ?>">Мастера</a></li>
        <li><a class="btn-form primary secondary<?=isActive('accruals')?>" href="<?= Url::toRoute(['accruals/index']) ?>">Начисления</a></li>
        <li><a class="btn-form primary secondary<?=isActive('settings')?>" href="<?= Url::toRoute(['settings/index']) ?>">Настройки</a></li>
    <?php endif; ?>
</ul>
