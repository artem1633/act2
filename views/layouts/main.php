<?php

use yii\helpers\Html;

/** @var string $content */


app\assets\AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&amp;display=swap" rel="stylesheet">
    <?=$this->head()?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="wrapper">
    <main class="main">
        <section class="lk-section" style="background-image: url('/images/bg-statistic.png');">
            <div class="container">
                <div class="row">
                    <div class="col-xs-2">
                        <div class="lk-nav">
                            <div class="logo-wrap">
                                <h1>ПО-БРАТСКИ</h1>
                                <h2>МУЖСКАЯ ПАРИКМАХЕРСКАЯ</h2>
                            </div>
                            <?= $this->render('menu') ?>
                        </div>
                    </div>
                    <div class="col-xs-8">
                        <div class="lk-content">
                            <?= $content ?>
                        </div>
                    </div>
                    <div class="col-xs-2">
                        <?php if(isset($this->params['rightContent'])): ?>
                            <?= $this->params['rightContent'] ?>
                        <?php else: ?>
                            <?php if(Yii::$app->user->identity->role != \app\models\User::ROLE_USER): ?>
                                <div class="bar-panel">
                                    <div class="bar bar_select">
                                        <div class="bar__wrap">
                                            <div class="bar__row">
                                                <div class="bar__icon"> <img src="/<?=Yii::$app->user->isGuest == false ? Yii::$app->user->identity->getRealAvatarPath() : ''?>" alt=""></div><span class="bar__text"><?= Yii::$app->user->identity->login ?></span><span class="bar__arrow"></span>
                                            </div>
                                            <div class="bar__drop">
                                                <div class="bar__drop-wrap"><a class="bar__text" href="<?= \yii\helpers\Url::toRoute(['/accruals/index']) ?>">Счет</a><a class="bar__text" href="<?= \yii\helpers\Url::toRoute(['/user/profile']) ?>">Профиль</a><a class="bar__text" data-method="post" href="<?= \yii\helpers\Url::toRoute(['site/logout']) ?>">Выйти</a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="bar-panel">
                                    <div class="bar bar_select">
                                        <div class="bar__wrap">
                                            <b style="line-height: 33px; padding: 0 8px; color: #5F9ABB;">Кредиты: <?= Yii::$app->user->isGuest == false ? Yii::$app->user->identity->company->credit : '' ?></b>
                                        </div>
                                    </div>
                                </div>
                                <p style="font-size: 11px;">Дата и время регистрации: <?= Yii::$app->user->isGuest == false ? Yii::$app->user->identity->company->created_date : '' ?></p>
                                <div class="bar-panel">
                                    <div class="bar bar_select">
                                        <div class="bar__wrap" style="font-size: 13px; padding: 11px;">
                                            <p>Логин: <?= Yii::$app->user->isGuest == false ? Yii::$app->user->identity->login : '' ?></p>
                                            <p>Приглашенных: <?= Yii::$app->user->isGuest == false ? \app\models\Companies::find()->where(['ref_id' => Yii::$app->user->identity->company->id])->count() : '' ?></p>
                                            <p>Приглашен от: <?php

                                                if(Yii::$app->user->isGuest == false){
                                                    if(Yii::$app->user->identity->company->ref_id){
                                                        $company = \app\models\Companies::findOne(Yii::$app->user->identity->company->ref_id);
                                                        if($company){
                                                            echo $company->company_name;
                                                        }
                                                    }
                                                }

                                                ?></p>
                                        </div>
                                    </div>
                                </div>
                                <a class="btn-form primary secondary" href="#" style="font-size: 1.3em; padding: 5px 20px;" data-ref-href="<?= \yii\helpers\Url::toRoute(['site/register', 'ref_id' => Yii::$app->user->identity->company_id], true) ?>" onclick="var link = $(this).attr('data-ref-href'); copyToClipboard(link); alert('Ссылка скопирована в буфер обмена');">Пригласить мастера</a>


<!--                        <div class="hidden">-->
                            <?php else: ?>
                            <div class="bar-panel">
                                <div class="bar bar_select">
                                    <div class="bar__wrap">
                                        <div class="bar__row">
                                            <div class="bar__icon"> <img src="/<?=Yii::$app->user->isGuest == false ? Yii::$app->user->identity->getRealAvatarPath() : ''?>" alt=""></div><span class="bar__text"><?= Yii::$app->user->identity->login ?></span><span class="bar__arrow"></span>
                                        </div>
                                        <div class="bar__drop">
                                            <div class="bar__drop-wrap"><a class="bar__text" href="<?= \yii\helpers\Url::toRoute(['/accruals/index']) ?>">Счет</a><a class="bar__text" href="<?= \yii\helpers\Url::toRoute(['/user/profile']) ?>">Профиль</a><a class="bar__text" data-method="post" href="<?= \yii\helpers\Url::toRoute(['site/logout']) ?>">Выйти</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bar-panel">
                                <div class="bar bar_select">
                                    <div class="bar__wrap">
                                        <b style="line-height: 33px; padding: 0 8px; color: #5F9ABB;">Кредиты: <?= Yii::$app->user->isGuest == false ? Yii::$app->user->identity->company->credit : '' ?></b>
                                    </div>
                                </div>
                            </div>
                            <div class="bar-panel">
                                <div class="bar bar_select">
                                    <div class="bar__wrap" style="font-size: 13px; padding: 11px;">
                                        <p>Логин: <?= Yii::$app->user->isGuest == false ? Yii::$app->user->identity->login : '' ?></p>
                                        <p>Приглашенных: <?= Yii::$app->user->isGuest == false ? \app\models\Companies::find()->where(['ref_id' => Yii::$app->user->identity->company->id])->count() : '' ?></p>
                                    </div>
                                </div>
                            </div>
                            <a class="btn-form primary secondary" href="<?= \yii\helpers\Url::toRoute(['/operations/index']) ?>" style="font-size: 1.2em; padding: 5px 14px;">Зафиксировать продажу</a>
<!--                        </div>-->
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </section>
    </main>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>