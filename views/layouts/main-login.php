<?php

use yii\helpers\Html;

/** @var string $content */


app\assets\AppAsset::register($this);

?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&amp;display=swap" rel="stylesheet">
    <?=$this->head()?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="wrapper">
    <main class="main">
        <section class="form-section" style="background-image: url('/images/bg-1.png');">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <?= $content ?>
                    </div>
                </div>
            </div>
        </section>
    </main>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>