<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

/** @var $this yii\web\View */
/** @var $dataProvider yii\data\ArrayDataProvider */

$this->title = 'Главная';

\app\assets\plugins\EqualsHeightAsset::register($this);

$pages = new \yii\data\Pagination(['totalCount' => $dataProvider->count]);

?>

    <div class="board_sorting"><span class="board__title">Сортировка:</span>
        <a class="btn-sorter" href="<?= \yii\helpers\Url::toRoute(['dashboard/index', 'UserSearch[sorting]' => 1]) ?>" type="button">По чел.</a>
        <a class="btn-sorter" href="<?= \yii\helpers\Url::toRoute(['dashboard/index', 'UserSearch[sorting]' => 2]) ?>" type="button">По $</a>
    </div>

    <div class="team">

<?php foreach ($dataProvider->models as $model): ?>

    <?php
    $user = \app\models\User::find()->where(['company_id' => $model['id'], 'role' => 2])->one();
    if($user){
        $path = \app\helpers\AvatarResolver::getRealAvatarPath($user->avatar);
    } else {
        $path = \app\helpers\AvatarResolver::getRealAvatarPath(null);
    }
    ?>

                <div class="team-card" style="margin-bottom: 20px;">
                    <div class="team-card_wrap">
                        <div class="team-card_avatar"><a href="/<?= \yii\helpers\Url::toRoute(['user/view', 'id' => $model['id']]) ?>"></a><img src="/<?=$path?>" alt=""></div>
                        <div class="team-card_content">
                            <div class="team-card_content_wrap">
                                <div class="team-card_content-info"><span class="team-card_content-name"><?= $model['company_name'] ?></span><span class="team-card_content-text">У<?=$model['level']?> чел. $<?= $model['credit'] ?></span></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="team-card_wrap">
                    <div class="team-card"></div>
                </div>

<?php endforeach; ?>
    </div>


    <div class="col-md-12">
        <?= \yii\widgets\LinkPager::widget([
            'pagination' => $pages,
        ]) ?>
    </div>

<?php

$script = <<< JS

$('#user-main-form input').change(function(){
    $('#user-main-form').submit();
});

$('.card-content-text').equalHeights();

JS;

$this->registerJs($script, \yii\web\View::POS_READY);


?>