<?php

/**
 * @var $this \yii\web\View
 * @var $settings \app\models\Settings[]
 */

$this->title = 'Настройки';

?>

<style>
    .board .form-group {
        border-bottom: 1px solid #fff;
        margin-bottom: 0;
    }


    .board .form-group input {
        height: 25px;
        font-size: 12px;
        margin-top: 11px;
    }

    .board .form-group .control-label {
        font-size: 14px;
        font-weight: 500;
        color: #5F9ABB;
        border-right: 1px solid #fff;
        padding-top: 14px;
        padding-bottom: 14px;
    }
</style>


<div class="board board_main">
    <div class="board-btns"><span class="board__title masters">Настройки</span>
    </div>
    <div class="board__wrap">
                <?php if (count($settings) > 0) { ?>
                    <form method="post" class="form-horizontal form-bordered" style="padding: 0;">
                        <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>"
                               value="<?= Yii::$app->request->csrfToken ?>">
                        <?php foreach ($settings as $setting): ?>
                            <div class="form-group">
                                <label for=""
                                       class="control-label col-md-3"><?= Yii::$app->formatter->asRaw($setting->label) ?></label>
                                <div class="col-md-9">
                                    <div class="col-md-8">
                                        <?php if($setting->type == null || $setting->type == \app\models\Settings::TYPE_TEXT): ?>
                                            <input name="Settings[<?= $setting->key ?>]" type="text"
                                                   value="<?= $setting->value ?>" class="form-control">
                                        <?php endif; ?>
                                        <?php if($setting->type == \app\models\Settings::TYPE_CHECKBOX): ?>
                                            <input name="Settings[<?= $setting->key ?>]" type="checkbox"
                                                <?= ($setting->value == 1 ? 'checked=""' : '') ?> class="switch">
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                        <div class="form-group">
                            <label for="" class="control-label col-md-3"></label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <button type="submit" style="margin-top: 2px; margin-left: 15px;"
                                            class="btn-form lk settings">Сохранить
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                <?php } else { ?>
                    <div class="note note-danger" style="margin: 15px;">
                        <h4><i class="fa fa-exclamation-triangle"></i> Настроек в базе данных не обнаружено!
                        </h4>
                        <p>
                            Убедитесь, что все миграции выполнены без ошибок
                        </p>
                    </div>
                <?php } ?>
    </div>
</div>