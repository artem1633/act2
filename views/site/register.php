<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Регистрация';

$fieldOptions1 = [
    'horizontalCssClasses' => [
        'error' => 'form__valid-text',
    ],
    'inputTemplate' => "<div class='form__field'>{input}</div>"
];

if(isset($_GET['ref_id'])){
    $refId = $_GET['ref_id'];
} else {
    $refId = null;
}


$model->ref_id = $refId;

?>

<style>
    label.control-label {
        color: #9e9e9e;
        font-weight: 400;
    }

    .field-signupform-work_status .form__field label{
        color: #9e9e9e;
    }
</style>

<div class="form-wrap">
    <div class="logo-wrap">
        <h1>ПО-БРАТСКИ</h1>
        <h2>МУЖСКАЯ ПАРИКМАХЕРСКАЯ</h2>
    </div>
    <label class="control-label" for="">Регистрация</label>
    <?php $form = ActiveForm::begin(['id' => 'register-form', 'enableClientValidation' => false], $options = ['class' => 'margin-bottom-0']); ?>
    <?= $form
        ->field($model, 'name', $fieldOptions1)
        ->label(false)
        ->textInput(['placeholder' => $model->getAttributeLabel('name'), 'class' => 'form__input']) ?>
    <?= $form
        ->field($model, 'email', $fieldOptions1)
        ->label(false)
        ->textInput(['placeholder' => $model->getAttributeLabel('email'), 'class' => 'form__input']) ?>
    <?= $form
        ->field($model, 'password', $fieldOptions1)
        ->label(false)
        ->passwordInput(['placeholder' => $model->getAttributeLabel('password'), 'class' => 'form__input']) ?>
    <?= $form
        ->field($model, 'birth_date', $fieldOptions1)
        ->label()
        ->textInput(['type' => 'date', 'class' => 'form__input'],['placeholder' => $model->getAttributeLabel('birth_date')]) ?>
    <?= $form
        ->field($model, 'country', $fieldOptions1)
        ->label(false)
        ->textInput(['placeholder' => $model->getAttributeLabel('country'), 'class' => 'form__input']) ?>
    <?= $form
        ->field($model, 'living_country', $fieldOptions1)
        ->label(false)
        ->textInput(['placeholder' => $model->getAttributeLabel('living_country'), 'class' => 'form__input']) ?>
    <?= $form
        ->field($model, 'living_place', $fieldOptions1)
        ->label(false)
        ->textInput(['placeholder' => $model->getAttributeLabel('living_place'), 'class' => 'form__input']) ?>
    <?= $form
        ->field($model, 'education', $fieldOptions1)
        ->label(false)
        ->textInput(['placeholder' => $model->getAttributeLabel('education'), 'class' => 'form__input']) ?>
    <?= $form
        ->field($model, 'profession', $fieldOptions1)
        ->label(false)
        ->textInput(['placeholder' => $model->getAttributeLabel('profession'), 'class' => 'form__input']) ?>
    <?= $form
        ->field($model, 'work_place', $fieldOptions1)
        ->label(false)
        ->textInput(['placeholder' => $model->getAttributeLabel('work_place'), 'class' => 'form__input']) ?>
    <?= $form
        ->field($model, 'work_position', $fieldOptions1)
        ->label(false)
        ->textInput(['placeholder' => $model->getAttributeLabel('work_position'), 'class' => 'form__input']) ?>

    <?= $form
        ->field($model, 'work_status', $fieldOptions1)
        ->label($model->getAttributeLabel('work_status'))
        ->radioList(['да' => 'да',
            'нет' => 'нет',
            'возможно' => 'возможно',
        ],
            [
                'item' => function($index, $label, $name, $checked, $value) {

                    $return = '<label class="form__label form__label_radio" onclick="$(\'.form__label_radio_checked\').removeClass(\'form__label_radio_checked\'); $(this).addClass(\'form__label_radio_checked\')">';
                    $return .= '<input class="" type="radio" name="' . $name . '" value="' . $value . '" tabindex="'. $index .'">';
                    $return .= '<i></i>';
                    $return .= '<span>' . ucwords($label) . '</span>';
                    $return .= '</label>';

                    return $return;
                }
            ]
        ) ?>

    <?= $form
        ->field($model, 'phone', $fieldOptions1)
        ->label(false)
        ->textInput(['placeholder' => $model->getAttributeLabel('phone'), 'class' => 'form__input']) ?>
    <?= $form
        ->field($model, 'interests', $fieldOptions1)
        ->label(false)
        ->textInput(['placeholder' => $model->getAttributeLabel('interests'), 'class' => 'form__input']) ?>
    <?= $form
        ->field($model, 'hobby', $fieldOptions1)
        ->label(false)
        ->textInput(['placeholder' => $model->getAttributeLabel('hobby'), 'class' => 'form__input']) ?>

    <?= $form
        ->field($model, 'ref_id', $fieldOptions1)
        ->label(false)
        ->textInput(['placeholder' => $model->getAttributeLabel('ref_id'), 'class' => 'form__input']) ?>

    <div class="form__footer">
        <?= Html::submitButton('Регистрация', ['class' => 'btn-form primary', 'name' => 'login-button']) ?>
    </div>

    <div class="login-buttons">
    </div>
    <?php ActiveForm::end(); ?>
</div>
