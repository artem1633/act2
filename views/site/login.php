<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Авторизация';

$fieldOptions1 = [
    'inputTemplate' => "<div class='form__field'>{input}</div>"
];

$fieldOptions2 = [
    'inputTemplate' => "<div class='form__field'>{input}</div>"
];
?>



<div class="form-wrap">
    <div class="logo-wrap">
        <h1>ПО-БРАТСКИ</h1>
        <h2>МУЖСКАЯ ПАРИКМАХЕРСКАЯ</h2>
    </div>
    <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false]); ?>
        <div class="form__wrap">
            <label class="form__label" for="#">Введите данные для авторизации </label>
            <?= $form
                ->field($model, 'username', $fieldOptions1)
                ->label(false)
                ->textInput(['value'=>'admin','placeholder' => $model->getAttributeLabel('email'), 'class' => 'form__input']) ?>

            <?= $form
                ->field($model, 'password', $fieldOptions2)
                ->label(false)
                ->passwordInput(['value'=>'admin','placeholder' => $model->getAttributeLabel('password'), 'class' => 'form__input']) ?>

            <div class="form__nav"><a class="form__link" href="<?= \yii\helpers\Url::toRoute(['site/forget-password']) ?>">Забыли пароль</a><a class="form__link" href="<?= \yii\helpers\Url::toRoute(['site/register']) ?>">Регистрация</a></div>
            <div class="form__footer">
                <?= Html::submitButton('Войти', ['class' => 'btn-form primary', 'name' => 'login-button']) ?>
            </div>
        </div>
    <?php ActiveForm::end(); ?>
</div>



