<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\forms\ForgetPassword */

$this->title = 'Востановить пароль';

$fieldOptions1 = [
    'inputTemplate' => "<div class='form__field'>{input}</div>"
];
?>

<div class="form-wrap">
    <div class="logo-wrap">
        <h1>ПО-БРАТСКИ</h1>
        <h2>МУЖСКАЯ ПАРИКМАХЕРСКАЯ</h2>
    </div>
    <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false], $options = ['class' => 'margin-bottom-0']); ?>
    <?= $form
        ->field($model, 'email', $fieldOptions1)
        ->label(false)
        ->textInput(['placeholder' => $model->getAttributeLabel('email'), 'class' => 'form__input']) ?>
    <div class="login-buttons">
        <?= Html::submitButton('Отправить новый пароль', ['class' => 'btn-form primary small', 'name' => 'login-button']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
