<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Accruals;
use yii\bootstrap\Modal;

/** @var $this \yii\web\View */

$this->title = 'Вывод Credit';

$credit = 0;

if(Yii::$app->user->isGuest == false){
    $now = date('Y-m-d');
    $lastMonth = date('Y-m-d', time() - (86400 * 30));
    if(Yii::$app->user->identity->isSuperAdmin() || Yii::$app->user->identity->isManager()){
        $credit = array_sum(ArrayHelper::getColumn(Accruals::find()->where(['for_company_id' => Yii::$app->user->identity->company_id])->andWhere(['between', 'date', "{$lastMonth} 00:00:00", "{$now} 23:59:59"])->all(), 'amount'));
    }
}

\johnitvn\ajaxcrud\CrudAsset::register($this);

?>

<div class="board">
    <div class="board__wrap board__wrap_withdraw">
        <div class="board__title board__title_withdraw">Уважаемые партнеры!</div>
        <p class="withdraw-text">В настоящие время сервис по выводу бонусов осуществляет выплаты на банковские карты и расчетные счета РФ. Ведутся работы по восстановлению выплат в страны СНГ</p>
        <ul class="withdraw-list">
            <li>Минимальный перевод бонуса 100 доллоров</li>
            <li>Финансовый период по начислению бонусов: четверг 00:00 — среда 24:00</li>
            <li>Закрытие недельного финансового периода происходит в каждую серду в 24:00</li>
        </ul><a class="withdraw-link" href="#" download="">Инструкция по выводу денег <span>PDF</span></a>
    </div>
</div>

<div class="board">
    <div class="board__wrap board__wrap_withdraw">
        <div class="withdraw-content">
            <div class="withdraw-balance">Общий баланс: <?= $credit ?></div>
            <a class="btn-form lk withdraw" href="<?= \yii\helpers\Url::toRoute(['credit-request/request']) ?>" type="button" role="modal-remote">Оставить заявку на вывод средств<img src="images/icon-money.png" alt=""></a>
        </div>
    </div>
</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>

