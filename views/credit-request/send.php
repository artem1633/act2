<?php

/** @var $model \app\models\forms\CreditRequestForm */

use yii\widgets\ActiveForm;

?>

<?php $form = ActiveForm::begin() ?>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'text')->textarea() ?>
        </div>
    </div>

<?php ActiveForm::end() ?>


