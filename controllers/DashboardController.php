<?php

namespace app\controllers;

use app\behaviors\UpdateOnlineBehavior;
use app\models\UserSearch;
use Yii;
use yii\helpers\VarDumper;
use yii\web\Controller;

/**
 * UserController implements the CRUD actions for User model.
 */
class DashboardController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            ['class' => UpdateOnlineBehavior::className()],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
        ];
    }

    /**
 * Render Yandex maps.
 * @return mixed
 */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->searchChild(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Render Yandex maps.
     * @return mixed
     */
    public function actionParent()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->searchParent(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionTest()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->searchByRating();

        VarDumper::dump($dataProvider->models, 10, true);
    }
}
