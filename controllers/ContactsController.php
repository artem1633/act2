<?php

namespace app\controllers;

use app\behaviors\UpdateOnlineBehavior;
use yii\web\Controller;

/**
 * Class ContactsController
 * @package app\controllers
 */
class ContactsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            ['class' => UpdateOnlineBehavior::className()],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
        ];
    }

    /**
     * Render Yandex maps.
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index', [

        ]);
    }
}