<?php

namespace app\controllers;

use app\models\Accruals;
use app\models\forms\ReportForm;
use app\models\Operations;
use app\models\Report;
use app\models\ReportSearch;
use Yii;
use app\models\SubscriptionSearch;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\filters\VerbFilter;

/**
 * ReportController implements the CRUD actions for Subscription model.
 */
class ReportController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Subscription models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new ReportForm();
        $model->load(Yii::$app->request->get());

        $data = $model->report();

        $now = date('Y-m-d');
        $lastWeek = date('Y-m-d', time() - (86400 * 7));
        $lastMonth = date('Y-m-d', time() - (86400 * 30));

        if(Yii::$app->user->identity->isSuperAdmin() || Yii::$app->user->identity->isManager()){
            $creditAll = array_sum(ArrayHelper::getColumn(Accruals::find()->where(['for_company_id' => Yii::$app->user->identity->company_id, 'user_level' => 1])->all(), 'amount'));
            $creditWeek = array_sum(ArrayHelper::getColumn(Accruals::find()->where(['for_company_id' => Yii::$app->user->identity->company_id, 'user_level' => 1])->andWhere(['between', 'date', "{$lastWeek} 00:00:00", "{$now} 23:59:59"])->all(), 'amount'));
            $creditMonth = array_sum(ArrayHelper::getColumn(Accruals::find()->where(['for_company_id' => Yii::$app->user->identity->company_id, 'user_level' => 1])->andWhere(['between', 'date', "{$lastMonth} 00:00:00", "{$now} 23:59:59"])->all(), 'amount'));

            $creditAllB = array_sum(ArrayHelper::getColumn(Accruals::find()->where(['for_company_id' => Yii::$app->user->identity->company_id, 'user_level' => 2])->all(), 'amount'));
            $creditWeekB = array_sum(ArrayHelper::getColumn(Accruals::find()->where(['for_company_id' => Yii::$app->user->identity->company_id, 'user_level' => 2])->andWhere(['between', 'date', "{$lastWeek} 00:00:00", "{$now} 23:59:59"])->all(), 'amount'));
            $creditMonthB = array_sum(ArrayHelper::getColumn(Accruals::find()->where(['for_company_id' => Yii::$app->user->identity->company_id, 'user_level' => 2])->andWhere(['between', 'date', "{$lastMonth} 00:00:00", "{$now} 23:59:59"])->all(), 'amount'));

            $creditAllC = array_sum(ArrayHelper::getColumn(Accruals::find()->where(['for_company_id' => Yii::$app->user->identity->company_id, 'user_level' => 3])->all(), 'amount'));
            $creditWeekC = array_sum(ArrayHelper::getColumn(Accruals::find()->where(['for_company_id' => Yii::$app->user->identity->company_id, 'user_level' => 3])->andWhere(['between', 'date', "{$lastWeek} 00:00:00", "{$now} 23:59:59"])->all(), 'amount'));
            $creditMonthC = array_sum(ArrayHelper::getColumn(Accruals::find()->where(['for_company_id' => Yii::$app->user->identity->company_id, 'user_level' => 3])->andWhere(['between', 'date', "{$lastMonth} 00:00:00", "{$now} 23:59:59"])->all(), 'amount'));

            $creditAllD = array_sum(ArrayHelper::getColumn(Accruals::find()->where(['for_company_id' => Yii::$app->user->identity->company_id, 'user_level' => 4])->all(), 'amount'));
            $creditWeekD = array_sum(ArrayHelper::getColumn(Accruals::find()->where(['for_company_id' => Yii::$app->user->identity->company_id, 'user_level' => 4])->andWhere(['between', 'date', "{$lastWeek} 00:00:00", "{$now} 23:59:59"])->all(), 'amount'));
            $creditMonthD = array_sum(ArrayHelper::getColumn(Accruals::find()->where(['for_company_id' => Yii::$app->user->identity->company_id, 'user_level' => 4])->andWhere(['between', 'date', "{$lastMonth} 00:00:00", "{$now} 23:59:59"])->all(), 'amount'));

            $debitAll = 0;
            $debitWeek = 0;
            $debitMonth = 0;
        } else {
            $debitAll = array_sum(ArrayHelper::getColumn(Operations::find()->where(['user_id' => Yii::$app->user->identity->id])->all(), 'sum'));
            $debitWeek = array_sum(ArrayHelper::getColumn(Operations::find()->where(['user_id' => Yii::$app->user->identity->id])->andWhere(['between', 'created_at', "{$lastWeek} 00:00:00", "{$now} 23:59:59"])->all(), 'sum'));
            $debitMonth = array_sum(ArrayHelper::getColumn(Operations::find()->where(['user_id' => Yii::$app->user->identity->id])->andWhere(['between', 'created_at', "{$lastMonth} 00:00:00", "{$now} 23:59:59"])->all(), 'sum'));

            $creditAll = 0;
            $creditWeek = 0;
            $creditMonth = 0;

            $creditAllB = 0;
            $creditWeekB = 0;
            $creditMonthB = 0;

            $creditAllC = 0;
            $creditWeekC = 0;
            $creditMonthC = 0;

            $creditAllD = 0;
            $creditWeekD = 0;
            $creditMonthD = 0;
        }

        return $this->render('index', [
            'searchModel' => $model,
            'subscribersReport' => $data,
            'creditAll' => $creditAll,
            'creditWeek' => $creditWeek,
            'creditMonth' => $creditMonth,
            'creditAllB' => $creditAllB,
            'creditWeekB' => $creditWeekB,
            'creditMonthB' => $creditMonthB,
            'creditAllC' => $creditAllC,
            'creditWeekC' => $creditWeekC,
            'creditMonthC' => $creditMonthC,
            'creditAllD' => $creditAllD,
            'creditWeekD' => $creditWeekD,
            'creditMonthD' => $creditMonthD,
            'debitAll' => $debitAll,
            'debitWeek' => $debitWeek,
            'debitMonth' => $debitMonth,
        ]);
    }
}